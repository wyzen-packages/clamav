# Wyzen ClamAV PHP Client

PHP Client to connect to ClamAV daemon over TCP or using a local socket from command line and scan your storage files for viruses.

Fork from https://github.com/appwrite/php-clamav to use `fsockopen` against `Socket object`

## Getting Started

Needs Clamav installed.

Install using composer:
```bash
composer require wyzen-packages/php-clamav
```

```php
<?php

require_once 'vendor/autoload.php';

use Wyzen\Php\ClamAV\Network;

$clam = new Network('localhost', 3310); // Or use new Pipe() for unix socket

$clam->ping(); // Check ClamAV is up and running

$clam->version(); // Check ClamAV version

$clam->fileScan('path/to/file.dmg'); // Returns true if a file is clean or false if a file is infected

$clam->reload(); // Reload ClamAV database

$clam->shutdown(); // Shutdown ClamAV
```

## System Requirements

This package requires PHP 7.1 or later. We recommend using the latest PHP version whenever possible.

## Copyright and license

The MIT License (MIT) [http://www.opensource.org/licenses/mit-license.php](http://www.opensource.org/licenses/mit-license.php)
