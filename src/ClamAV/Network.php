<?php

/**
 * Wyzen PHP Framework
 *
 * @package ClamAV
 *
 */

namespace Wyzen\ClamAV;

use RuntimeException;

class Network extends ClamAV
{
    /**
     * @var string
     */
    private const CLAMAV_HOST = '127.0.0.1';

    /**
     * @var int
     */
    private const CLAMAV_PORT = 3310;

    /**
     * @var string
     */
    private string $host;

    /**
     * @var int
     */
    private int $port;

    /**
     * Network constructor
     *
     * You need to pass the host address and the port the server.
     *
     * @param string $host
     * @param int $port
     */
    public function __construct(string $host = self::CLAMAV_HOST, int $port = self::CLAMAV_PORT)
    {
        $this->host = $host;
        $this->port = $port;
    }

    /**
     * Returns a remote socket.
     *
     * @return resource
     * @throws \Exception
     */
    protected function getSocket()
    {
        $socket = @fsockopen($this->host, $this->port, $errno, $errstr, 1);
        if ($socket === false) {
            throw new \Exception(self::ERR_COM_CLAMAV);
        }
        return $socket;
    }
}
