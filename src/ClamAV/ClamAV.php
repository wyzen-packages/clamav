<?php

/**
 * Wyzen PHP Framework
 *
 * @package ClamAV
 */

namespace Wyzen\ClamAV;

abstract class ClamAV
{
    /**
     * @var int
     */
    public const CLAMAV_MAX = 20000;
    public const OK='OK';
    public const FILE_NOT_FOUND='FILE_NOT_FOUND';
    public const ERR_COM_CLAMAV = 'Error communicating with ClamAV';

    /**
     * @return resource
     */
    abstract protected function getSocket();

    /**
     * Send a given command to ClamAV.
     *
     * @param string $command
     * @return string
     */
    private function sendCommand(string $command)
    {
        $socket = $this->getSocket();

        fwrite($socket, $command, \strlen($command));
        $return = fread($socket, self::CLAMAV_MAX);
        fclose($socket);
        return \trim($return);
    }

    /**
     * Check if ClamAV is up and responsive.
     *
     * @return bool
     */
    public function ping(): bool
    {
        $return = $this->sendCommand('PING');

        return \trim($return) === 'PONG';
    }

    /**
     * Check ClamAV Version.
     *
     * @return string
     */
    public function version(): string
    {
        return \trim($this->sendCommand('VERSION'));
    }

    /**
     * Reload ClamAV virus databases.
     *
     * @return string|null
     */
    public function reload(): ?string
    {
        return $this->sendCommand('RELOAD');
    }

    /**
     * Shutdown ClamAV and preform a clean exit.
     *
     * @return string|null
     */
    public function shutdown(): ?string
    {
        return $this->sendCommand('SHUTDOWN');
    }

    /**
     * Scan a file or a directory (recursively) with archive support
     * enabled (if not disabled in clamd.conf). A full path is required.
     *
     * Returns whether the given file/directory is clean (true), or not (false).
     *
     * @param string $file
     * @return string
     */
//    public function fileScanInStream(string $file): string
//    {
//        $socket = $this->getSocket();
//
//        $handle = @\fopen($file, 'rb');
//        if ($handle===false){
//            return self::FILE_NOT_FOUND;
//        }
//        $chunkSize = \filesize($file) < 8192 ? \filesize($file) : 8192;
//        $command = "zINSTREAM\0";
//
//        fwrite($socket, $command, \strlen($command));
//        while (!\feof($handle)) {
//            $data = \fread($handle, $chunkSize);
//            $packet = \pack(\sprintf("Na%d", $chunkSize), $chunkSize, $data);
//            fwrite($socket, $packet,$chunkSize + 4);
//        }
//
//        fwrite($socket, \pack("Nx", 0), 5);
//        $out = fread($socket, self::CLAMAV_MAX);
//        fclose($socket);
//
//        $out = \explode(':', $out);
//        $stats = trim(\end($out));
//
//        return $stats === 'OK' ? self::OK : $stats;
//    }

    /**
     * Scan a file or a directory (recursively) with archive support
     * enabled (if not disabled in clamd.conf). A full path is required.
     *
     * Returns whether the given file/directory is clean (true), or not (false).
     *
     * @param string $file
     * @return string
     */
    public function fileScan(string $file): string
    {
        $out = $this->sendCommand('SCAN ' . $file);

        if (strpos($out, "No such file or directory")!==false){
            return self::FILE_NOT_FOUND;
        }

        $out = \explode(':', $out);
        $stats = \trim(\end($out));

        return $stats === 'OK' ? self::OK : $stats;
    }

    /**
     * Scan file or directory (recursively) with archive support
     * enabled, and don't stop the scanning when a virus is found.
     *
     * @param string $file
     * @return array
     */
    public function continueScan(string $file): array
    {
        $return = [];

        foreach (\explode("\n", \trim($this->sendCommand('CONTSCAN ' . $file))) as $results) {
            [$file, $stats] = \explode(':', $results);
            $return[] = ['file' => $file, 'stats' => \trim($stats)];
        }

        return $return;
    }
}
