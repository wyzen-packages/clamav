<?php

/**
 * Wyzen PHP Framework
 *
 * @package ClamAV
 *
 */

namespace Wyzen\ClamAV;

class Pipe extends ClamAV
{
    /**
     * @var string
     */
    private const CLAMAV_HOST = '/run/clamav/clamd.sock';

    /**
     * @var string
     */
    private string $pip;

    /**
     * Pipe constructor.
     *
     * This class can be used to connect to local socket.
     * You need to pass the path to the socket pipe.
     *
     * @param string $pip
     */
    public function __construct(string $pip = self::CLAMAV_HOST)
    {
        $this->pip = $pip;
    }

    /**
     * Returns a local socket.
     *
     * @return resource
     */
    protected function getSocket()
    {
        $socket = fopen($this->pip, 'rb');
        return $socket;
    }
}
